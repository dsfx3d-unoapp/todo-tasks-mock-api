# TODO APP API

The To-Do List app allows a user to create, read, update or delete to-do tasks.

[**Documentaion**](https://bit.ly/2r64EUp) | [**Frontend Specs**](https://bit.ly/35nbDqG)

A to-do task:

* has a title
* has a description
* has a boolean flag completed to represent current state

Task Object:

  ```json
  {
   "id": 1,
   "order": 1,
   "title": "title of the task",
   "description": "description of the task",
   "completed": false,
   "userId": 1
  }
   ```

**Important:** A mock API for hiring purposes.

## Usage

Install dependencies:

`npm install`

Start mock server:

`npm run serve`
